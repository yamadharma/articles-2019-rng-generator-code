#include "../include/randomgen.hpp"

void lcg(std::vector<unsigned long long int>& V, unsigned long long int seed) {
  // m = 2^64 - 1 так как 0 тоже учитывается
  const unsigned long long int m = 18446744073709551615llu;
  const unsigned long long int a = 6364136223846793005llu;
  const unsigned long long int c = 1442695040888963407llu;

  for (auto& v: V) {
   seed = (a * seed + c) % m; 
   v = seed;
  }
}

unsigned long long int lcg(unsigned long long int& seed) {
  // m = 2^64 - 1 так как 0 тоже учитывается
  const unsigned long long int m = 18446744073709551615llu;
  const unsigned long long int a = 6364136223846793005llu;
  const unsigned long long int c = 1442695040888963407llu;

  seed = (a * seed + c) % m;
  return seed;
}