#include "../include/randomgen.hpp"

// kiss
unsigned long long int kiss(std::vector<unsigned long long int>& seed) {
  // x = seed[0]; y = seed[1]; z = seed[2]; c = seed[3];
  unsigned long long int t;
  const unsigned long long int a = 698769069llu;
  
  seed[0] = 69069 * seed[0] + 123456;
  seed[1] = seed[1] ^ (seed[1] << 13);
  seed[1] = seed[1] ^ (seed[1] >> 17);
  seed[1] = seed[1] ^ (seed[1] <<  5);
  t = a * seed[2] + seed[3];
  seed[3] = (t >> 32);
  seed[2] = t;
  return seed[0] + seed[1] + seed[2];
}

unsigned long long int jkiss(std::vector<unsigned long long int>& seed) {
  // x = seed[0]; y = seed[1]; z = seed[2]; c = seed[3];
  unsigned long long int t;
  seed[0] = 314527869 * seed[0] + 1234567;
  seed[1] = seed[1] ^ (seed[1] <<  5);
  seed[1] = seed[1] ^ (seed[1] >>  7);
  seed[1] = seed[1] ^ (seed[1] << 22);
  t = 4294584393llu * seed[2] + seed[3];
  seed[3] = t >> 32;
  seed[2] = t;
  return seed[0] + seed[1] + seed[2];
}