#include "../include/randomgen.hpp"

// xorshift*
unsigned long long int  xorshift64star(unsigned long long int seed) {
  unsigned long long int x;
  x = seed;
  x = x ^ (x >> 12);
  x = x ^ (x << 25);
  x = x ^ (x >> 27);
  return x * 2685821657736338717ull;
}

// xorshift+
unsigned long long int  xorshift128plus(std::vector<unsigned long long int>& seed) {
  unsigned long long int x = seed[0];
  const unsigned long long int y = seed[1];
  
  seed[0] = y;
  x = x ^ (x << 23);
  seed[1] = x ^ y ^ (x >> 17) ^ (y >> 26);
  // Массив передается в функцию по ссылке, поэтому значения аргумента seed[] меняются
  return seed[1] + y;
}

// xorshift
unsigned long long int  xorshift(std::vector<unsigned long long int>& seed) {
  unsigned long long int t;
  t = seed[0];
  t = t ^ (t << 11);
  t = t ^ (t >> 8);
  seed[0] = seed[1];
  seed[1] = seed[2];
  seed[2] = seed[3];
  seed[3] = seed[3] ^ (seed[3] >> 19);
  seed[3] = seed[3] ^ t;
  return seed[3];
}