#include <iostream>
#include <vector>
#include <string>

// xorshift.cpp
unsigned long long int  xorshift64star(unsigned long long int seed);
unsigned long long int  xorshift128plus(std::vector<unsigned long long int>& seed);
unsigned long long int  xorshift(std::vector<unsigned long long int>& seed);

// kiss.cpp
unsigned long long int kiss(std::vector<unsigned long long int>& seed);
unsigned long long int jkiss(std::vector<unsigned long long int>& seed);

void lcg(std::vector<unsigned long long int>& V, unsigned long long int seed);
unsigned long long int lcg(unsigned long long int& seed);