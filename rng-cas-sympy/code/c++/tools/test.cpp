#include "../include/randomgen.hpp"

enum rng_code {
  KISS, JKISS,
  XORSHIFT_STAR, XORSHIFT_PLUS, XORSHIFT,
  LCG
};

rng_code rng_string (const std::string& rng) {
  if (rng == "kiss") return KISS;
  if (rng == "jkiss") return JKISS;
  if (rng == "xorshift*") return XORSHIFT_STAR;
  if (rng == "xorshift+") return XORSHIFT_PLUS;
  if (rng == "xorshift") return XORSHIFT;
  if (rng == "lcg") return LCG;
  return XORSHIFT;
}

int main(int argc, char *argv[]) {
  
  unsigned long int N;
  rng_code generator_name;

  if (argc > 2) {
    generator_name = rng_string(std::string(argv[1]));
    N = std::stoull(argv[2]);
  }
  else {
    std::cerr << "Введите название генератора и количество чисел, которое следует сгенерировать!" << std::endl;
    return -1;
  }
  
  const auto bytes_num = sizeof(unsigned long long int);
  unsigned long long int iseed;
  std::vector<unsigned long long int> seed;
  
  switch(generator_name) {
    case KISS:
      seed.resize(4);
      for (auto& s: seed) {
        std::cin.read((char *) &s, bytes_num);
      }
      for (unsigned long long int i=0; i<N; ++i) {
        std::cout << kiss(seed) << std::endl;
      }
    break;
    case JKISS:
      seed.resize(4);
      for (auto& s: seed) {
        std::cin.read((char *) &s, bytes_num);
      }
      for (unsigned long long int i=0; i<N; ++i) {
        std::cout << jkiss(seed) << std::endl;
      }
    break;
    case XORSHIFT_STAR:
      std::cin.read((char *) &iseed, bytes_num);
      for (unsigned long long int i=0; i<N; ++i) {
        std::cout << (iseed = xorshift64star(iseed)) << std::endl;
      }
    break;
    case XORSHIFT_PLUS:
      seed.resize(2);
      for (auto& s: seed) {
        std::cin.read((char *) &s, bytes_num);
      }
      for (unsigned long long int i=0; i<N; ++i) {
        std::cout << xorshift128plus(seed) << std::endl;
      }
    break;
    case XORSHIFT:
      seed.resize(4);
      for (auto& s: seed) {
        std::cin.read((char *) &s, bytes_num);
      }
      for (unsigned long long int i=0; i<N; ++i) {
        std::cout << xorshift(seed) << std::endl;
      }
    break;
    case LCG:
      std::cin.read((char *) &iseed, bytes_num);
      for (unsigned long long int i=0; i<N; ++i) {
        std::cout << lcg(iseed) << std::endl;
      }
    break;
  }
  return 0;
}
