#include <iostream>
#include <string>

// Альтернативный подход --- перенаправить вывод из
// /dev/urandom в нашу программу и считать ровно 8
// байт, записав их в файл. В этом случае ненужно
// открывать файл из программы и начальное значение
// можно передать не только из /dev/urandom, но и
// ввести вручную или из файла/другой программы.

unsigned long long int LCG(unsigned long long int& seed) {
  const unsigned long long int m = 18446744073709551615llu;
  const unsigned long long int a = 6364136223846793005llu;
  const unsigned long long int c = 1442695040888963407llu;

  seed = (a * seed + c) % m;
  return seed;
}

int main(int argc, char *argv[]) {

  unsigned long int N;

  if (argc > 1) {
    N = std::stoul(argv[1]);
  }
  else {
    std::cerr << "Введите количество чисел, которое следует сгенерировать!" << std::endl;
    return -1;
  }

  unsigned long long int seed;
  const auto bytes_num = sizeof seed;
  // считываем seed из стандартного ввода
  std::cin.read((char *) &seed, bytes_num);

  for (auto i=0; i<N; ++i){
    std::cout << LCG(seed) << std::endl;
  }
  // для считывания начального значения из /dev/urandom
  // запускаем из консоли
  // ./a.out 10 < /dev/urandom > data.txt
  // альтернатива --- воспользоваться конвеером
  // cat /dev/urandom | ./a.out 100 > data.txt
  return 0;
}

