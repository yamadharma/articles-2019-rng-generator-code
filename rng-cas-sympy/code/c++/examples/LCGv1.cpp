#include <iostream>
#include <fstream>
#include <string>

// функция принимает аргумент по ссылке, так как нам
// необходимо его изменять
unsigned long long int LCG(unsigned long long int& seed) {
  // m = 2^64 - 1 так как 0 тоже учитывается
  // обязательно добавляем llu (long long unsigned)
  // после числа иначе предупреждение компилятора
  const unsigned long long int m = 18446744073709551615llu;
  const unsigned long long int a = 6364136223846793005llu;
  const unsigned long long int c = 1442695040888963407llu;

  seed = (a * seed + c) % m;
  return seed;
}

int main(int argc, char *argv[]) {

  unsigned long int N;

  // первый аргумент командной строки --- количество генерируемых
  // псевдослучайных чисел.
  if (argc > 1) {
    // конвертируем его в целый беззнаковый тип
    // функцией из библиотеки string (стандарт С++11)
    N = std::stoul(argv[1]);
  }
  else {
    // если аргумент не передан прекращаем работу программы
    std::cerr << "Введите количество чисел, которое следует сгенерировать!" << std::endl;
    return -1;
  }

  // открываем файл /dev/urandom на чтение в бинарном режиме
  std::ifstream devrand;
  devrand.open("/dev/urandom", std::ios::in | std::ios::binary);

  // начальное значение для генератора
  unsigned long long int seed;
  // определяем количество байт в типе переменной seed
  const auto bytes_num = sizeof seed;
  // альтернативный способ
  // const auto bytes_num = sizeof(unsigned long long int);

  // записываем из /dev/urandom bytes_num байт в seed
  devrand.read((char *) &seed, bytes_num);
  devrand.close();
  // можно проверить, что bytes_num == 8
  // std::cout << bytes_num << std::endl;
  // std::cout << seed << std::endl;

  // наконец генерируем желаемое количество чисел
  for (auto i=0; i<N; ++i){
    std::cout << LCG(seed) << std::endl;
  }
  // сгенерированные числа печатаем в стандартный вывод
  // столбиком; если нужно сохранить вывод в файл, то
  // используем перенаправление стандартного вывода `>`
  // ./a.out 1000 > data.txt
  return 0;
}

