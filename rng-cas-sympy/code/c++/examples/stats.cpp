#include <vector>
#include <algorithm>
#include <numeric>
#include <iostream>

using namespace std;

double max(vector <double> V) {
  auto M = max_element(V.begin(), V.end());
  return *M;
}

double min(vector <double> V) {
  auto M = min_element(V.begin(), V.end());
  return *M;
}


double sum(const vector <double>& V) {
  double res = 0;
  for(const auto& v : V) {
    res += v;
  }
  return res;
}

double avg(vector <double> V) {
  return sum(V) / double(V.size());
}

int main(int argc, char* argv[]) {
  
  vector <double> V = {3.0, 1.0, 5.0};
  
  cout << "max(V) = " << max(V) << endl;
  cout << "min(V) = " << min(V) << endl;
  cout << "sum(V) = " << sum(V) << endl;
  
  double S = accumulate(V.begin(), V.end(), 0);
  cout << "accumulate(V) = " << S << endl;
  
  cout << "avg(V) = " << avg(V) << endl;
  
  return 0;
}