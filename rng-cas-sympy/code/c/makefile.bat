﻿::@echo off
chcp 65001
F:
cd "F:\\Программирование\\C и C++\\random\\c"

:: Параметры компилятора
set CFLAGS=-O3 -Werror -Wall -std=c11 -fPIC -I./include
:: Дополнительные библиотеки (файл librandomgen, но ключ -lrandomgen)
set LDLIBS=-lrandomgen -lm

gcc -c %CFLAGS% src/devrandom.c -o lib/devrandom.o
gcc -c %CFLAGS% src/lcg.c -o lib/lcg.o
gcc -c %CFLAGS% src/lfg.c -o lib/lfg.o
gcc -c %CFLAGS% src/icg.c -o lib/icg.o
gcc -c %CFLAGS% src/kiss.c -o lib/kiss.o
gcc -c %CFLAGS% src/mt.c -o lib/mt.o
gcc -c %CFLAGS% src/xorshift.c -o lib/xorshift.o
gcc -c %CFLAGS% src/xoroshiro.c -o lib/xoroshiro.o
gcc -c %CFLAGS% src/normal.c -o lib/normal.o
gcc -c %CFLAGS% src/poisson.c -o lib/poisson.o
gcc -c %CFLAGS% src/processes.c -o lib/processes.o
gcc -c %CFLAGS% src/exponential.c -o lib/exponential.o
gcc -c %CFLAGS% src/adapters.c -o lib/adapters.o

gcc -shared lib/devrandom.o lib/lcg.o lib/lfg.o lib/icg.o lib/kiss.o lib/mt.o lib/xorshift.o lib/xoroshiro.o lib/adapters.o lib/normal.o lib/poisson.o lib/processes.o lib/exponential.o -o bin/librandomgen.dll -Wl,--out-implib,lib/win/librandomgen.a

gcc -c %CFLAGS% tools/test.c -o lib/test.o
gcc ./lib/test.o -Wl,-rpath=/lib/win -L./lib/win %LDLIBS% -o bin/random.exe

pause