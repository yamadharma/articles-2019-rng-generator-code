#include "../include/randomgen.h"

#define PRINT_INFO 0 // Печать отладочных данных
#define PRINT_NUMS 0 // Печать целых чисел, а не потока байтов
#define DIEHARD_FORMAT 0 //  Печать в формате для тестов DieHarder

/* ----------------------------------------------------------
*  Формат вызова программы
* ./test -G gen_num -N Num -S seed1,seed2,seed3
* ----------------------------------------------------------
* Некоторые генераторы требуют для инициализации больше одного числа,
* поэтому для seed значений выделяем массив. Большинству генераторов
* нужно 1 или 2 числа, только LFG требует около 60 чисел.
* Отметим также, что seed используется для хранения состояния генератора
* и при каждом вызове функции изменяется (например, в LFG). Это возможно
* благодаря тому, что массив передается в функцию по ссылке не по значению.
*/

// Тестируемые генераторы
typedef enum {
  LCG = 1,
  LCG2 = 2, 
  LFG = 3,
  XORSHIFT_STAR = 4,
  XORSHIFT_PLUS = 5,
  XORSHIFT = 6,
  MT = 7,
  KISS = 8,
  JKISS = 9,
  ICG = 10,
  XOROSHIRO_128_PLUS = 11,
  XOSHIRO_256_PLUS = 12,
  XOSHIRO_256_STAR_STAR = 13
} generator_type;

int main(int argc, char *argv[]) {
  // функция генератора
  generator gen;
  // Номер генератора который мы хотим сгенерировать
  generator_type generator_num = KISS;
  // количество чисел, которые надо сгенерировать
  uint64_t N = 10ull;
  // нормировать последовательность (т.е. приводить к виду [0, 1))
  int normed = 0;
  // Количество элементов в массиве seed
  int const seed_size = 60;
  // Заполнялись ли сиды вручную?
  int seed_set = 0;
  // Выделяем место для seed'ов
  uint64_t seed[seed_size];
  // Заполняем нулями, чтобы знать, сколько seed'ов
  // было заполнено вручную
  memset(seed, 0, sizeof(seed));

  // Разбираем аргументы
  static const char *optString = "G:N:S:n";
  char* end;
  int opt;

  while ((opt = getopt(argc, argv, optString)) != -1) {
    switch (opt) {
      case 'G': generator_num = strtol(optarg, &end, 10); break;
      case 'N': N = strtoull(optarg, &end, 10); break;
      case 'n': normed = 1; break;
      case 'S': {
        uint64_t i = 0;
        char* p = strtok(optarg, ",;");
        // заполняем сиды, пока есть данные 
        // И место в массиве seeds
        while ((p != NULL) && (seed_size > i)) {
          seed[i++] = strtoull(p, &end, 10);
          // Нужно для продвижения дальше по строке
          p = strtok(NULL, ",;");
        }
        // сиды устанавливались вручную
        seed_set = 1;
      }
      break;
      // case '1': p1 = strtold(optarg, &end); break;
      // case '2': p2 = strtold(optarg, &end); break;
    }
  }

  // Если seed не инициализирован, то набираем значения с /dev/urandom
  if (seed_set == 0) {
    for(int i=0; i<seed_size; seed[i++] = devurandom(8));
  }

  switch (generator_num) {
    // линейный конгруэнтный метод
    case LCG: gen = lcg; break;
    // комбинация двух линейных конгруэнтных методов
    case LCG2: gen = lcg2; break;
    // Запаздывающий генератор Фибоначчи (lagged Fibonacci generator, lfg)
    case LFG: gen = lfg; break;
    //  xorshift*
    case XORSHIFT_STAR: gen = xorshift64star; break;
    // xorshift+
    case XORSHIFT_PLUS: gen = xorshift128plus; break;
    // xorshift
    case XORSHIFT: gen = xorshift; break;
    // Mersenne Twister
    case MT: gen = mersenne_twister; break;
    // KISS
    case KISS: gen = kiss; break;
    // JKISS
    case JKISS: gen = jkiss; break;
    // Инверсный конгруэнтный генератор
    case ICG: gen = icg; break;
    // xoroshiro128+
    case XOROSHIRO_128_PLUS: gen = xoroshiro128plus; break;
    // xoroshiro256+
    case XOSHIRO_256_PLUS: gen = xoshiro256plus; break;
    // xoroshiro256**
    case XOSHIRO_256_STAR_STAR: gen = xoshiro256starstar; break;
    // По умолчанию
    default: gen = xorshift128plus; break;
  }

  #if PRINT_INFO > 0
    printf("# generator № %i\n", generator_num);
    printf("# N = %"PRIu64" \n", N);
  #endif
  #if PRINT_INFO > 1
    for(int i=0; seed[i]!=0; i++){
      printf("# seed[%i] = %"PRIu64" \n", i, seed[i]);
    }
  #endif
  #if DIEHARD_FORMAT > 0
    printf("type: d\ncount: %"PRIu64"\nnumbit: 64\n", N);
  #endif

  uint64_t x;

  for(uint64_t i=1; i<=N; i++) {
    // нормированная последовательность
    if (normed > 0) {
      printf("%.18G\n", normed_rand(gen, seed));
    }
    // только целые числа в бинарном или текстовом виде
    else {
        x = gen(seed);
      #if PRINT_NUMS > 0
        printf("%"PRIu64"\n", x);
      #else
        fwrite(&x, sizeof(x), 1, stdout);
      #endif
    }
  }

  return 0;
}
