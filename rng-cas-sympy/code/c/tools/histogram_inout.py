# -*- coding: utf-8 -*-
"""
Данная программа считывает числа и строит на их основе гистограмму
Данные должны иметь вид столбика целых чисел
"""
import sys
import matplotlib.pyplot as plt
import numpy as np

# Раскомментировать, если не отображаются кириллические буквы
plt.rcParams['font.sans-serif'] = ['Arial', 'DejaVu Sans']
plt.rcParams['font.serif'] = ['Times New Roman', 'DejaVu Serif']

random_numbers = np.loadtxt(sys.stdin)

deg = int(np.log10(random_numbers.size))

fig01 = plt.figure(1, figsize=(10, 5))
ax01 = fig01.add_subplot(1, 1, 1)

ax01.hist(random_numbers, bins=500, density=True, histtype='step')

ax01.set_title('Для $10^{{{0:d}}}$ псевдослучайных чисел'.format(deg))

if sys.version_info[0] >= 3:
  fig01.savefig(sys.stdout.buffer, format='png', dpi=300, bbox_inches='tight', pad_inches=0.1)
else:
  fig01.savefig(sys.stdout, format='png', dpi=300, bbox_inches='tight', pad_inches=0.1)
