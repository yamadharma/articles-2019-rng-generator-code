#! /usr/bin/env python3
# -*- codng: utf-8 -*-

import sys
import numpy as np
import PIL

from PIL import Image

random_numbers = np.loadtxt(sys.stdin)
N = random_numbers.size

rand = random_numbers / random_numbers.max()

# Создаем пустую квадратную картинку с 256 оттенками серого
# или иначе 8 бит на оттенки серого.
img = PIL.Image.new('L', (int(np.sqrt(N)), int(np.sqrt(N))), 60)

# Формируем массив пикселей из случайных чисел
pixels = np.rint(rand*255)
pixels.reshape((int(np.sqrt(N)), int(np.sqrt(N))))

# Записываем значения цвета пикселей в картинку
img.putdata(pixels)

if sys.version_info[0] >= 3:
  img.save(sys.stdout.buffer, format='png')
else:
  img.save(sys.stdout, format='png')
