'''Тестирование генераторов псевдослучайных равномерно распределенных
чисел на Си с помощью набора тестов DieHarder'''
#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# Не хотим проверки констант/переменых
# pylint: disable=C0103

import subprocess as sp

generators = {
    'LCG': {
        'name': "линейный конгруэнтный метод (LCG)",
        'arg': 1,
        'file': "./out/LCG.out"
    },
    'LCG2': {
        'name': "lcg2 -- комбинация двух линейных конгруэнтных методов",
        'arg': 2,
        'file': "./out/LCG2.out"
    },
    'LFG': {
        'name': "запаздывающий генератор Фибоначчи (LFG)",
        'arg': 3,
        'file': "./out/LFG.out"
    },
    'XorShiftStar': {
        'name': "xorshift*",
        'arg': 4,
        'file': "./out/XorShiftStar.out"
    },
    'XorShiftPlus': {
        'name': "xorshift+",
        'arg': 5,
        'file': "./out/XorShiftPlus.out"
    },
    'XorShift': {
        'name': "xorshift",
        'arg': 6,
        'file': "./out/XorShift.out"
    },
    'MT': {
        'name': "вихрь Мерсенна (Mersenne Twister)",
        'arg': 7,
        'file': "./out/MT.out"
    },
    'KISS': {
        'name': "kiss",
        'arg': 8,
        'file': "./out/kiss.out"
    },
    'JKISS': {
        'name': "jkiss",
        'arg': 9,
        'file': "./out/jkiss.out"
    },
    'ICG': {
        'name': "Инверсный конгруэнтный генератор",
        'arg': 10,
        'file': "./out/icg.out"
    },
    'normal': {
        'name': "Нормальное распределение методом Бокса--Мюллера",
        'arg': 11,
        'file': "./out/normal.out"
    },
    'exp': {
        'name': "Экспоненциальный генератор",
        'arg': 12,
        'file': "./out/exp.out"
    },
    'poisson': {
        'name': "Генератор распределения Пуассона",
        'arg': 13,
        'file': "./out/poisson.out"
    },
    'dev-urandom': {
        'name': "Псевдоустройство /dev/urandom",
        'arg': 99,
        'file': "./out/devurandom.out"
    }
}
order = 9
NUM = 2*(10**order)
procs = []
for generator, params in generators.items():
    print("Генерируем {0} чисел, используя генератор: {1}".format(NUM, params['name']))
    cmd = ['./main', str(params['arg']), str(NUM)]
    file = open(params['file'], 'w')
    procs.append(sp.call(cmd, stdout=file))
    file.close()

# for p in procs:
    # exit_code = p.wait()
    # print(exit_code)

procs = []
for generator, params in generators.items():
    print("Запускаем тест DieHarder для генератора: {0}".format(params['name']))
    cmd = ['dieharder', '-a', '-g', '202', '-f', params['file']]
    dhfile = open(params['file']+'.dh', 'w')
    procs.append(sp.call(cmd, stdout=dhfile))
    dhfile.close()

# for p in procs:
    # exit_code = p.wait()
    # print(exit_code)
