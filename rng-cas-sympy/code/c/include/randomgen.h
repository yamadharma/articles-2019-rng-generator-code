#ifdef _WIN32
  #define __USE_MINGW_ANSI_STDIO 1
#endif

#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <inttypes.h> // uint64_t
#include <stdlib.h> // strtol
#include <string.h> // strtok
#include <getopt.h> // getopt

// Структура для нормального распределения
struct Point {
  long double x;
  long double y;
};

// Сигнатура базовой функции-генератора
typedef uint64_t (*generator)(uint64_t*);

// devrandom.c
uint64_t devurandom(uint64_t byte_num);
uint64_t devrandom(uint64_t byte_num);

// xoroshiro.c
uint64_t xoroshiro128plus(uint64_t seed[static 2]);
uint64_t xoshiro256plus(uint64_t seed[static 4]);
uint64_t xoshiro256starstar(uint64_t seed[static 4]);


// xorshift.c
uint64_t xorshift64star(uint64_t seed[static 1]);
uint64_t xorshift128plus(uint64_t seed[static 2]);
uint64_t  xorshift(uint64_t seed[static 4]);

// lcg.c
uint64_t lcg(uint64_t seed[static 1]);
uint64_t lcg2(uint64_t seed[static 2]);

// mt.c
uint64_t mersenne_twister(uint64_t seed[static 1]);

// lfg.c
uint64_t lfg(uint64_t x[static 55]);
uint64_t lfg_prod(uint64_t x[static 55]);

// kiss.c
uint64_t kiss(uint64_t seed[static 4]);
uint64_t jkiss(uint64_t seed[static 4]);


// icg.c
uint64_t icg(uint64_t seed[]);

void icg_N (uint64_t seed[], uint64_t N, uint64_t res[]);

// normal.c
struct Point normal(long double mu, long double sigma, uint64_t seed[]);

// exponential.c
long double exponential(long double lambda, uint64_t seed[]);

// poisson.c
int64_t poisson(generator gen, long double lambda, uint64_t seed[]);
int64_t poisson2(generator gen, long double lambda, uint64_t seed[]);
int64_t poisson3(generator gen, long double lambda, uint64_t seed[]);

// processes.c
void wiener_print(long double mu, long double sigma, uint64_t seed[], uint64_t N);
void poisson_process_print(long double lambda, uint64_t seed[], uint64_t N);

// adapters.c
double normed_rand(generator gen, uint64_t seed[]);
void rand_n(generator gen, uint64_t N, uint64_t seed[], uint64_t res[]);
void normed_rand_n(generator gen, uint64_t N, uint64_t seed[], double res[]);

// ----------------------------------------------------
// Для удобного вызова из python с использованием ctypes
// ----------------------------------------------------
// Нормированные генераторы
double normed_lcg(uint64_t seed[static 1]);

double normed_kiss(uint64_t seed[static 4]);
double normed_jkiss(uint64_t seed[static 4]);

double normed_mersenne_twister(uint64_t seed[static 1]);

double normed_xorshift(uint64_t seed[static 4]);
double normed_xorshift128plus(uint64_t seed[static 2]);
double normed_xorshift64star(uint64_t seed[static 1]);

double normed_xoroshiro128plus(uint64_t seed[static 2]);
double normed_xoshiro256plus(uint64_t seed[static 4]);
double normed_xoshiro256starstar(uint64_t seed[static 4]);

// целочисленные заполнители
void lcg_n(uint64_t N, uint64_t seed[static 1], uint64_t res[]);

void kiss_n(uint64_t N, uint64_t seed[static 4], uint64_t res[]);
void jkiss_n(uint64_t N, uint64_t seed[static 4], uint64_t res[]);

void mersenne_twister_n(uint64_t N, uint64_t seed[static 1], uint64_t res[]);

void xorshift_n(uint64_t N, uint64_t seed[static 4], uint64_t res[]);
void xorshift64star_n(uint64_t N, uint64_t seed[static 1], uint64_t res[]);
void xorshift128plus_n(uint64_t N, uint64_t seed[static 2], uint64_t res[]);

void xoroshiro128plus_n(uint64_t N, uint64_t seed[static 2], uint64_t res[]);
void xoshiro256plus_n(uint64_t N, uint64_t seed[static 4], uint64_t res[]);
void xoshiro256starstar_n(uint64_t N, uint64_t seed[static 4], uint64_t res[]);

// Нормированные double заполнители
void normed_lcg_n(uint64_t N, uint64_t seed[], double res[]);

void normed_kiss_n(uint64_t N, uint64_t seed[], double res[]);
void normed_jkiss_n(uint64_t N, uint64_t seed[], double res[]);

void normed_mersenne_twister_n(uint64_t N, uint64_t seed[], double res[]);

void normed_xorshift_n(uint64_t N, uint64_t seed[], double res[]);
void normed_xorshift64star_n(uint64_t N, uint64_t seed[], double res[]);
void normed_xorshift128plus_n(uint64_t N, uint64_t seed[], double res[]);

void normed_xoroshiro128plus_n(uint64_t N, uint64_t seed[static 2], double res[]);
void normed_xoshiro256plus_n(uint64_t N, uint64_t seed[static 4], double res[]);
void normed_xoshiro256starstar_n(uint64_t N, uint64_t seed[static 4], double res[]);