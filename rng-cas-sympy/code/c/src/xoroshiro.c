#include "../include/randomgen.h"
/*
* Based on code by David Blackman 
* and Sebastiano Vigna (vigna@acm.org) 
* 2016-2018 oshiro.di.unimi.it
*/

// вспомогательная функция
static inline uint64_t rotl(const uint64_t x, int k) {
    return (x << k) | (x >> (64 - k));
}

// xoshiro128+
// (a=24, b=16, b=37) --- actual version
// (a=55, b=14, c=36) -- 2016 version
// S = 2x64 = 128
uint64_t xoroshiro128plus(uint64_t seed[static 2]) {
    
    const uint64_t s0 = seed[0];
    uint64_t s1 = seed[1];
    
    const uint64_t result = s0 + s1;

    s1 = s1 ^ s0;
    seed[0] = rotl(s0, 24) ^ s1 ^ (s1 << 16); // a, b
    seed[1] = rotl(s1, 37); // c

    return result;
}

// xoshiro256+
// S = 4x64 = 256
uint64_t xoshiro256plus(uint64_t seed[static 4]) {

	const uint64_t res = seed[0] + seed[3];
	const uint64_t t = seed[1] << 17;

	seed[2] ^= seed[0];
	seed[3] ^= seed[1];
	seed[1] ^= seed[2];
	seed[0] ^= seed[3];

	seed[2] ^= t;

	seed[3] = rotl(seed[3], 45);

	return res;
}

// xoshiro256**
// S = 4x64 = 256
uint64_t xoshiro256starstar(uint64_t seed[static 4]) {

	const uint64_t res = rotl(seed[1] * 5, 7) * 9;

	const uint64_t t = seed[1] << 17;

	seed[2] ^= seed[0];
	seed[3] ^= seed[1];
	seed[1] ^= seed[2];
	seed[0] ^= seed[3];

	seed[2] ^= t;

	seed[3] = rotl(seed[3], 45);

	return res;
}