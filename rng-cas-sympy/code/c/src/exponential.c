#include "../include/randomgen.h"
/// Генерация последовательности псевдослучайных экспоненциально распределенных чисел
long double exponential_(generator gen, long double lambda, uint64_t seed[]) {
  long double u  = normed_rand(gen, seed);
  while (1) {
    u = normed_rand(gen, seed);
    // если число из (0, 1) то прерываем цикл
    if ((0 < u) && (u < 1)) {
      break;
    };
  }
  // -(1.0 / lambda) * log(1.0 - u);
  // или
  // -1.0 / (lambda * log(u));
  return -1.0 / (lambda * log(u));
}

/// упрощенный вариант, без возможности выбора генератора
long double exponential(long double lambda, uint64_t seed[]) {
  return exponential_(xorshift, lambda, seed);
}