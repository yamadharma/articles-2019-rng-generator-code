#include "../include/randomgen.h"

/* Функции, использующие базовые генераторы для
*  генерации равномерно-распредленных чисел из интервала
*  [0, 1).
*  Также функции, заполняющие переданный массив числами
*/


/// Нормированный генератор равномерно распределенных
/// псевдослучайных чисел. Дает `double` число из интервала
/// [0, 1)
double normed_rand(generator gen, uint64_t seed[]) {
    return (double) gen(seed) / (double) UINT64_MAX;
}

/// Заполнение массива целыми числами
void rand_n(generator gen, uint64_t N, uint64_t seed[], uint64_t res[]) {
    for (uint64_t i=0; i < N; ++i) {
        res[i] = gen(seed);
    }
}

/// Заполнение массива вещественными числами
void normed_rand_n(generator gen, uint64_t N, uint64_t seed[], double res[]) {
    for (uint64_t i=0; i < N; ++i) {
        res[i] = normed_rand(gen, seed);
    }
}


/// Интерфейсы для использования в Python
/// через ctypes только для основных генераторов

// ---------------------------------------------
//  Нормированные генераторы
// ---------------------------------------------
double normed_lcg(uint64_t seed[static 1]) {
    return normed_rand(lcg, seed);
}

double normed_kiss(uint64_t seed[static 4]) {
    return normed_rand(kiss, seed);
}

double normed_jkiss(uint64_t seed[static 4]) {
    return normed_rand(jkiss, seed);
}

double normed_mersenne_twister(uint64_t seed[static 1]) {
    return normed_rand(mersenne_twister, seed);
}

double normed_xorshift(uint64_t seed[static 4]) {
    return normed_rand(xorshift, seed);
}

double normed_xorshift64star(uint64_t seed[static 1]) {
    return normed_rand(xorshift64star, seed);
}

double normed_xorshift128plus(uint64_t seed[static 2]) {
    return normed_rand(xorshift128plus, seed);
}

double normed_xoroshiro128plus(uint64_t seed[static 2]) {
    return normed_rand(xoroshiro128plus, seed);
}

double normed_xoshiro256plus(uint64_t seed[static 4]) {
    return normed_rand(xoshiro256plus, seed);
}

double normed_xoshiro256starstar(uint64_t seed[static 4]) {
    return normed_rand(xoshiro256starstar, seed);
}

// ---------------------------------------------
//  Заполнители целочисленных массивов
// ---------------------------------------------
void lcg_n(uint64_t N, uint64_t seed[static 1], uint64_t res[]) {
    rand_n(lcg, N, seed, res);
}

void kiss_n(uint64_t N, uint64_t seed[static 4], uint64_t res[]) {
    rand_n(kiss, N, seed, res);
}

void jkiss_n(uint64_t N, uint64_t seed[static 4], uint64_t res[]) {
    rand_n(jkiss, N, seed, res);
}

void mersenne_twister_n(uint64_t N, uint64_t seed[static 1], uint64_t res[]) {
    rand_n(mersenne_twister, N, seed, res);
}

void xorshift_n(uint64_t N, uint64_t seed[static 4], uint64_t res[]) {
    rand_n(xorshift, N, seed, res);
}

void xorshift64star_n(uint64_t N, uint64_t seed[static 1], uint64_t res[]) {
    rand_n(xorshift64star, N, seed, res);
}

void xorshift128plus_n(uint64_t N, uint64_t seed[static 2], uint64_t res[]) {
    rand_n(xorshift128plus, N, seed, res);
}

void xoroshiro128plus_n(uint64_t N, uint64_t seed[static 2], uint64_t res[]) {
    rand_n(xoroshiro128plus, N, seed, res);
}

void xoshiro256plus_n(uint64_t N, uint64_t seed[static 4], uint64_t res[]) {
    rand_n(xoshiro256plus, N, seed, res);
}

void xoshiro256starstar_n(uint64_t N, uint64_t seed[static 4], uint64_t res[]){
    rand_n(xoshiro256starstar, N, seed, res);
}
// ---------------------------------------------
//  Заполнители double массивов
// ---------------------------------------------
void normed_lcg_n(uint64_t N, uint64_t seed[static 1], double res[]) {
    normed_rand_n(lcg, N, seed, res);
}

void normed_kiss_n(uint64_t N, uint64_t seed[static 4], double res[]) {
    normed_rand_n(kiss, N, seed, res);
}

void normed_jkiss_n(uint64_t N, uint64_t seed[static 4], double res[]) {
    normed_rand_n(jkiss, N, seed, res);
}

void normed_mersenne_twister_n(uint64_t N, uint64_t seed[static 1], double res[]) {
    normed_rand_n(mersenne_twister, N, seed, res);
}

void normed_xorshift_n(uint64_t N, uint64_t seed[static 4], double res[]) {
    normed_rand_n(xorshift, N, seed, res);
}

void normed_xorshift64star_n(uint64_t N, uint64_t seed[static 1], double res[]) {
    normed_rand_n(xorshift64star, N, seed, res);
}

void normed_xorshift128plus_n(uint64_t N, uint64_t seed[static 2], double res[]) {
    normed_rand_n(xorshift128plus, N, seed, res);
}

void normed_xoroshiro128plus_n(uint64_t N, uint64_t seed[static 2], double res[]) {
    normed_rand_n(xoroshiro128plus, N, seed, res);
}

void normed_xoshiro256plus_n(uint64_t N, uint64_t seed[static 4], double res[]) {
    normed_rand_n(xoshiro256plus, N, seed, res);
}

void normed_xoshiro256starstar_n(uint64_t N, uint64_t seed[static 4], double res[]){
    normed_rand_n(xoshiro256starstar, N, seed, res);
}