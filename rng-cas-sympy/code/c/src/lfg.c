#include "../include/randomgen.h"


// Запаздывающий генераторы Фибоначчи (lagged Fibonacci generator, lfg)
// Функции надо передать массив длины минимум max(na, nb) + 1, причем все
uint64_t lfg(uint64_t x[static 55]){
  int i;
  int n;
  // магические переменные генератора, задающие лаг
  const int na = 55;
  const int nb = 24;
  
  n = (na > nb) ? na + 1 : nb + 1;
  
  // может происходить переполнение, что нормально
	// это равносильно использованию m = 2^64
  x[n] = x[n-na] - x[n-nb];
  
  for(i=0; i<=(n-1); i++){
    x[i] = x[i+1];
  }
  return x[n];
}

uint64_t lfg_prod(uint64_t x[static 55]){
  int i;
  int n;
  // магические переменные генератора, задающие лаг
  const int na = 55;
  const int nb = 24;
  
  n = (na > nb) ? na + 1 : nb + 1;
  
  // то же, что и в предыдущей функции, только
	// вместо умножения произведение
  x[n] = x[n-na] * x[n-nb];
  
  for(i=0; i<=(n-1); i++){
    x[i] = x[i+1];
  }
  return x[n];
}