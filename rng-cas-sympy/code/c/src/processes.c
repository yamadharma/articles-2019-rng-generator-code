#include "../include/randomgen.h"

/// Генерация винеровского процесса с распечаткой
void wiener_print(long double mu, long double sigma, uint64_t seed[], uint64_t N){
  struct Point w, w_tmp;
  w.x = 0.0;
  w.y = 0.0;

  for(uint64_t i=1; i<=N/2; i++) {
    w_tmp = normal(mu, sigma, seed);
    w.x += w_tmp.x;
    w.y += w_tmp.y;
    printf("%0.18LG\n%0.18LG\n", w.x, w.y);
  };
  // Функция normal возвращает два числа, поэтому
  // нужно отдельно предусмотреть случай, когда
  // N нечетно и вернуть на последнем шаге только одно число
  if (N % 2 == 1) {
    w.x += normal(mu, sigma, seed).x;
    printf("%0.18LG\n", w.x);
  }
};

/// Генерация пуассоновского процесса с распечаткой
void poisson_process_print(long double lambda, uint64_t seed[], uint64_t N){
  int64_t p = 0.0;
  for(uint64_t i=1; i<=N; i++) {
    p = p + poisson(xorshift, lambda, seed);
    printf("%"PRIu64" \n", p);
  };
};