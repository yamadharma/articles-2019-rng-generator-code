#include "../include/randomgen.h"
/*
* https://en.wikipedia.org/wiki/Xorshift
* From Xorshift RNGs, George Marsaglia
*/

// xorshift32
// S = 1x32, T = 2^32 - 1
uint32_t xorshift32(uint32_t seed[static 1]) {
  uint32_t x = seed[0];
  x = x ^ (x << 13);
  x = x ^ (x >> 17);
  x = x ^ (x << 5);
  seed[0] = x;
  return x;
}

// xorshift64
// S = 1x64, T = 2^64 - 1
uint64_t xorshift64(uint64_t seed[static 1]) {
	uint64_t x = seed[0];
	x = x ^ (x << 13);
	x = x ^ (x >> 7);
	x = x ^ (x << 17);
  seed[0] = x;
	return x;
}

// xorshift128
// S = 4x32, T = 2^128 - 1
uint32_t xorshift128(uint32_t seed[static 4]) {
	uint32_t s, t;
  t = seed[3];
	seed[3] = seed[2];
	seed[2] = seed[1];
  s = seed[0];
  seed[1] = s;
	t = t ^ (t << 11);
	t = t ^ (t >> 8);
  seed[0] = t ^ s ^ (s >> 19);
	return seed[0];
}

// xorwow
// T = 2^160 - 2^32
uint32_t xorwow (uint32_t seed[static 5]) {
  uint32_t s;
  uint32_t t = seed[3];
  seed[3] = seed[2];
  seed[2] = seed[1];
  s = seed[0];
  seed[1] = s;
  t = t ^ (t >> 2);
  t = t ^ (t << 1);
  t = t ^ (s ^ (s << 4));
  seed[0] = t;
  seed[4] += 362437;
  return t + seed[4];
}

// xorshift*
// S = 1x64, T = 2^64-1
uint64_t  xorshift64star (uint64_t seed[static 1]) {
  uint64_t x;
  x = seed[0];
  x = x ^ (x >> 12);
  x = x ^ (x << 25);
  x = x ^ (x >> 27);
  seed[0] = x;
  return x * 2685821657736338717ull;
}

// xorshift+
// S = 2x64, T = 2^128 - 1
uint64_t  xorshift128plus (uint64_t seed[static 2]) {
  uint64_t x = seed[0];
  const uint64_t y = seed[1];
  seed[0] = y;
  x = x ^ (x << 23);
  x = x ^ (x >> 17);
  x = x ^ (y ^ (y >> 26));
  seed[1] = x;
  return x + y;
}

// xorshift
// Откуда взят алгоритм? В википедии его нет или уже нет
uint64_t  xorshift (uint64_t seed[static 4]) {
  uint64_t t;
  t = seed[0];
  t = t ^ (t << 11);
  t = t ^ (t >> 8);
  seed[0] = seed[1];
  seed[1] = seed[2];
  seed[2] = seed[3];
  seed[3] = seed[3] ^ (seed[3] >> 19);
  seed[3] = seed[3] ^ t;
  return seed[3];
}