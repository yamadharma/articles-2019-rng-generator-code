#include "../include/randomgen.h"

///  Чтение случайных байт из /dev/urandom
uint64_t devurandom(uint64_t byte_num) {
  int file  = open("/dev/urandom", O_RDONLY);
  uint64_t res;

  if(file == -1)
    res = 0;
  else {
    int bn = read(file, &res, byte_num);
    if (bn != byte_num) {
      printf("Error!");
	  }
  }
  close(file);
  return res;
}

///  Чтение случайных байт из /dev/random
uint64_t devrandom(uint64_t byte_num) {
  int file  = open("/dev/random", O_RDONLY);
  uint64_t res;

  if(file == -1)
    res = 0;
  else {
    int bn = read(file, &res, byte_num);
    if (bn != byte_num) {
      printf("Error!");
	  }
  }
  close(file);
  return res;
}
