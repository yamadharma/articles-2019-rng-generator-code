#include "../include/randomgen.h"

/*
  Линейный конгруэнтный метод
  X[n+1] = (a*X[n] + c) mod m
  m --- модуль (m >= 2) mask
  a --- множитель (0 <= a < m) multiplier
  c --- приращение (0 <= c < m) addend
  x[0] --- начальное значение (seed)
*/
uint64_t lcg(uint64_t seed[static 1]) {

  const uint64_t a = 6364136223846793005;
  const uint64_t c = 1442695040888963407;
  // const uint64_t m = 18446744073709551615llu;
  
  uint64_t x = seed[0];
  
  x = (a * x + c) % UINT64_MAX;
  seed[0] = x;
  return x;

}

/*
  Комбинация двух линейных конгруэнтных методов
  X[n+1] = (a1*X[n] + c1) mod m
  Y[n+1] = (a2*Y[n] + c2) mod m
*/
uint64_t lcg2(uint64_t seed[static 2]){
  const uint64_t a = 6364136223846793005;
  const uint64_t c1 = 1442695040888963407;
  const uint64_t c2 = 1llu;
  // const uint64_t m = 18446744073709551615llu;

  uint64_t x, y;

  x = seed[0];
  y = seed[1];

  x = (a * x + c1) % UINT64_MAX;
  y = (a * x + c2) % UINT64_MAX;
  seed[0] = x;
  seed[1] = y;
  return (x - y) % UINT64_MAX;
}

// Еще можно сюда добавить квадратичный конгруэнтный метод
