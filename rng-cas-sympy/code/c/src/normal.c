#include "../include/randomgen.h"

/// Генерация последовательности псевдослучайных, нормально распределенных чисел
/// https://en.wikipedia.org/wiki/Box–Muller_transform
struct Point normal_(generator gen, long double mu, long double sigma, uint64_t seed[]) {

  // const uint64_t m = 18446744073709551615llu;
  //const long double pi = 3.14159265358979323846;
  long double u1, u2, s, x, y;
  // long double y;

  while (1) {
    // Сиды передаются в виде двумерного массива и перезаписываются внутри
    // Генерируем два случайных числа из интервала [0, 1)
    u1 = normed_rand(gen, seed);
    u2 = normed_rand(gen, seed);
    // Преобразуем их к интервалу [-1, 1]
    // u1 = a + (b - a) * u1, где 0 <= u1 <= 1
    // u1 = -1 + (1 + 1) * u1 = 2*u1 -1
    u1 = 2.0 * u1 - 1;
    u2 = 2.0 * u2 - 1;

    s = u1 * u1 + u2 * u2;
    // Если s > 1 и s = 0,то следует отбросить сгенерированные значения и
    // сгенерировать новые. Если же 0 < s <= 1, то значения подходят и
    // можно прервать цикл.
    if (s > 0 && s <= 1){
      break;
    };
  };
  x = u1 * sqrt(-2.0 * log(s) / s);
  y = u2 * sqrt(-2.0 * log(s) / s);
  struct Point res;
  res.x = sigma * x + mu;
  res.y = sigma * y + mu;
  return res;
}

struct Point normal(long double mu, long double sigma, uint64_t seed[]) {
  return normal_(xorshift, mu, sigma, seed);
}
