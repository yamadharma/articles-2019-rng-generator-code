#include "../include/randomgen.h"
/*
*  Функции основаны на псевдокоде
*  https://en.wikipedia.org/wiki/Poisson_distribution#Generating_Poisson-distributed_random_variables
*/
int64_t poisson(generator gen, long double lambda, uint64_t seed[]) {
  /*
  *  Алгоритм Кнтуа для генерирования пуассоновскго распределения
  */
  // const uint64_t m = 18446744073709551615llu;
  long double L, p, u;
  int64_t k;
  
  L = exp(-lambda);
  k = 0;
  p = 1.0;

  do {
    k = k + 1;
    u = normed_rand(gen, seed);
    p = p * u;
  } while(p > L);
  return k - 1;
}

int64_t poisson2(generator gen, long double lambda, uint64_t seed[]){
  /*
  *  Junhao, based on Knuth
  */
  // const uint64_t m = 18446744073709551615llu;
  const int STEP = 500;
  long double lambdaLeft, p, u, e;
  int64_t k;
  
  lambdaLeft = lambda;
  k = 0;
  p = 1.0;
  e = exp(1.0);

  do {
    k = k + 1;
    do{
      u = normed_rand(gen, seed);
    } while( u > 0.0 && u < 1.0);
    p = p * u;
    
    if ((p < e) && (lambdaLeft > 0)) {
      if (lambdaLeft > STEP) {
        p = p * exp(STEP);
        lambdaLeft = lambdaLeft - STEP;
      }
      else {
        p = p*exp(lambdaLeft);
        lambdaLeft = -1;
      }
    }
  } while(p > 1);
  return k - 1;
}

int64_t poisson3(generator gen, long double lambda, uint64_t seed[]){
  /*
  *  Poisson generator based upon the inversion by sequential search
  */
  // const uint64_t m = 18446744073709551615llu;
  long double p, s, u;
  int64_t x = 0;

  p = exp(-lambda);
  s = p;
  u = normed_rand(gen, seed);

  while(u > s){
    x = x + 1;
    p = p * lambda / (long double) x;
    s = s + p;
  }
  return x;
}