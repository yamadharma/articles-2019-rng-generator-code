#include "../include/randomgen.h"

// Вихрь Мерсенна (Mersenne twister, MT)
// стандарт MT19937-64
// псевдокод https://en.wikipedia.org/wiki/Mersenne_Twister
#define w 64
#define n 312
#define m 156
#define r 31
#define a 0xb5026f5aa96619e9
#define u 29
#define d 0x5555555555555555
#define s 17
#define b 0x71d67fffeda60000
#define t 37
#define c 0xfff7eee000000000
#define l 43
#define f 6364136223846793005
#define lowest_w_bits 0xffffffffffffffff

uint64_t MT[n-1];
// idx вместо index так как по видимому существует функция index
uint64_t idx = n + 1;

const uint64_t lower_mask = (1llu << r) - 1llu;
// upper_mask = lowest w bits of (not lower_mask)
const uint64_t upper_mask =  lowest_w_bits & (~((1llu << r) - 1llu));

static void seed_mt(uint64_t seed) {
  idx = n;
  MT[0] = seed;
  for(int i=1; i<=(n-1); i++) {
    MT[i] = lowest_w_bits & (f * (MT[i-1] ^ (MT[i-1] >> (w-2))) + i);
  }
}

static void twist(void) {
  uint64_t  x;
  uint64_t  xA;
  for(int i=0; i<=(n-1); i++) {
    x = (MT[i] & upper_mask) + (MT[(i+1) % n] & lower_mask);
    xA = x >> 1;
    if((x % 2) != 0){
      xA = xA ^ a;
    }
    MT[i] = MT[(i+m) % n] ^ xA;
  }
  idx = 0;
}

// extract_number
uint64_t mersenne_twister(uint64_t seed[static 1]) {
  uint64_t  y;
  
  if(idx >= n){
    if(idx > n){
      // printf("Генератор не инициализирован\n");
      // заменить константу на случайное число
      seed_mt(seed[0]);
    }
    twist();
  }
  y = MT[idx];
  y = y ^ ((y >> u) & d);
  y = y ^ ((y << s) & b);
  y = y ^ ((y << t) & c);
  y = y ^ (y >> l);
  
  ++idx;
  return lowest_w_bits & y;
}