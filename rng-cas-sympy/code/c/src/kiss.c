#include "../include/randomgen.h"

// KISS
uint64_t kiss(uint64_t seed[static 4]) {
  // x = seed[0]; y = seed[1]; z = seed[2]; c = seed[3];
  
  uint64_t a = 698769069llu;
  
  seed[0] = 69069*seed[0] + 123456;
  seed[1] = seed[1] ^ (seed[1] << 13);
  seed[1] = seed[1] ^ (seed[1] >> 17);
  seed[1] = seed[1] ^ (seed[1] <<  5);

  uint64_t t = a*seed[2] + seed[3];
  seed[3] = (t >> 32);
  seed[2] = t;
  return seed[0] + seed[1] + seed[2];
}

// jKISS
uint64_t jkiss(uint64_t seed[static 4]) {
  // x = seed[0]; y = seed[1]; z = seed[2]; c = seed[3];

  seed[0] = 314527869 * seed[0] + 1234567;
  seed[1] = seed[1] ^ (seed[1] <<  5);
  seed[1] = seed[1] ^ (seed[1] >>  7);
  seed[1] = seed[1] ^ (seed[1] << 22);

  uint64_t t = 4294584393llu * seed[2] + seed[3];

  seed[3] = t >> 32;
  seed[2] = t;

  return seed[0] + seed[1] + seed[2];
}