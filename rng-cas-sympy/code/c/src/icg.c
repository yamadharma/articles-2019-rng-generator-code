#include "../include/randomgen.h"

static int64_t inverse(int64_t a,  int64_t m){
  int64_t t;
  int64_t r;
  int64_t t_new, t_new_tmp;
  int64_t r_new, r_new_tmp;
  int64_t quotient;
  t = 0;
  r = m;
  t_new = 1;
  r_new = a;

  while (r_new != 0){
    quotient = r / r_new;

    t_new_tmp = t_new;
    t_new = t - quotient * t_new_tmp;
    t = t_new_tmp;

    r_new_tmp = r_new;
    r_new = r - quotient * r_new_tmp;
    r = r_new_tmp;
    }
  if(r > 1)
      return 0;
  if(t < 0)
      t = t + m;
  return t;
}

// Инверсный конгруэнтный генератор
uint64_t icg(uint64_t seed[]){
  const int64_t a = 1288490188;
  const int64_t c = 1;
  const int64_t m = pow(2, 61);
  if(seed == 0) {
    return c;
  }
  seed[0] = ((a * inverse(seed[0], m) + c) % m);
  return (uint64_t)seed[0];
}