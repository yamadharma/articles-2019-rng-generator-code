import ctypes
import numpy as np

from typing import Optional, Union, Type, Iterable, NoReturn
from random import randint

import sys
import os

path = os.path.dirname(os.path.realpath(__file__))

if sys.platform.startswith('win'):
    clib = ctypes.CDLL(os.path.join(path, 'librandomgen.dll'))
else:
    clib = ctypes.CDLL(os.path.join(path, 'librandomgen.so'))


class Random:

    def __init__(self, name: str = 'xorshift') -> NoReturn:
        """Класс-обертка над pure-C функциями по генерации
        псевдослучайных чисел.

        Parameters
        ----------
        name : string
            Название генератора, по умолчанию используется `xorshift*`. Доступны следующие варианты генераторов:
            - `lcg`
            - `kiss`
            - `jkiss`
            - `mersenne_twister`
            - `xorshift*`
            - `xorshift+`
            - `xorshift`
        """
        if name.lower() == 'lcg':
            self.name = name
            self.random_int = clib.lcg_n
            self.random_double = clib.normed_lcg_n
            self.random_int_step = clib.lcg
            self.random_double_step = clib.normed_lcg
            self.seeds_num = 1
        elif name.lower() == 'kiss':
            self.name = name
            self.random_int = clib.kiss_n
            self.random_double = clib.normed_kiss_n
            self.random_int_step = clib.kiss
            self.random_double_step = clib.normed_kiss
            self.seeds_num = 4
        elif name.lower() == 'jkiss':
            self.name = name
            self.random_int = clib.jkiss_n
            self.random_double = clib.normed_jkiss_n
            self.random_int_step = clib.jkiss
            self.random_double_step = clib.normed_jkiss
            self.seeds_num = 4
        elif name.lower() in ('mt', 'mt19937', 'mersenne_twister'):
            self.name = name
            self.random_int = clib.mersenne_twister_n
            self.random_double = clib.normed_mersenne_twister_n
            self.random_int_step = clib.mersenne_twister
            self.random_double_step = clib.normed_mersenne_twister
            self.seeds_num = 1
        elif name.lower() == 'xorshift*':
            self.name = name
            self.random_int = clib.xorshift64star_n
            self.random_double = clib.normed_xorshift64star_n
            self.random_int_step = clib.xorshift64star
            self.random_double_step = clib.normed_xorshift64star
            self.seeds_num = 1
        elif name.lower() == 'xorshift+':
            self.name = name
            self.random_int = clib.xorshift128plus_n
            self.random_double = clib.normed_xorshift128plus_n
            self.random_int_step = clib.xorshift128plus
            self.random_double_step = clib.normed_xorshift128plus
            self.seeds_num = 2
        elif name.lower() == 'xorshift':
            self.name = name
            self.random_int = clib.xorshift_n
            self.random_double = clib.normed_xorshift_n
            self.random_int_step = clib.xorshift
            self.random_double_step = clib.normed_xorshift
            self.seeds_num = 4
        else:
            print(f"Нет генератора с именем {name}!")
            self.name = 'xorshift'
            self.random_int = clib.xorshift_n
            self.random_double = clib.normed_xorshift_n
            self.random_int_step = clib.xorshift
            self.random_double_step = clib.normed_xorshift
            self.seeds_num = 4
        self.seed = None
        self.random_int.restype = None
        self.random_double.restype = None
        self.random_int_step.restype = ctypes.c_uint64
        self.random_double_step = ctypes.c_double
        # Для итератора
        self.iterator_count = 0
        self.iterator_size = 0
        self.iterator_type = int

    def set_seed(self, seed: Optional[Iterable] = None) -> __name__:
        """Инициализировать начальное значение генератора. В зависимости от типа
        генератора может понадобится от 1 до 56 целых чисел"""
        if seed is None:
            print("Используем `randomint` для инициализации `seed`")
            seed = [randint(0, 2**63) for _ in range(self.seeds_num)]

        self.seed = (ctypes.c_uint64 * self.seeds_num)(*seed[:self.seeds_num])

        return self

    def generate(self, size: int = 10,
                 type: Union[Type[int], Type[float]] = int) -> Optional[np.ndarray]:
        """Генерация случайных чисел"""
        if self.seed is None:
            self.set_seed()
        # Создаем пустой массив для передачи в функцию
        if type is int:
            res = (ctypes.c_uint64 * size)()
            function = self.random_int
        elif type is float:
            res = (ctypes.c_double * size)()
            function = self.random_double
        else:
            print("type mast be int or float!")
            return None

        size = ctypes.c_uint64(size)
        function(size, self.seed, res)

        if type is int:
            res = np.array(res, copy=False, dtype=np.uint64)
        elif type is float:
            res = np.array(res, copy=False, dtype=np.float64)

        # Не нужно обновлять seed, так как он передается по ссылке
        # и обновляется сам!
        return res

    def __call__(self, size: int = 10, type=int) -> np.ndarray:
        return self.generate(size, type)

    def __repr__(self) -> str:
        return f"Random(name='{self.name}')"

    def __str__(self) -> str:
        res = f"name: {self.name}, seeds: {self.seed}, " \
              f"seeds num: {self.seeds_num}"
        return res

    def set_iterator(self, size: int, type: Union[Type[int], Type[float]]) -> __name__:
        """Настройка итератора"""
        self.iterator_size = int(size)
        self.iterator_type = type
        self.iterator_count = 0
        return self

    def __next__(self) -> Union[int, float]:
        if self.seed is None:
            self.set_seed()
        # Итератор исчерпался или не был инициализирован
        if self.iterator_count >= self.iterator_size:
            raise StopIteration
        self.iterator_count += 1

        if self.iterator_type is int:
            return int(self.random_int_step(self.seed))
        elif self.iterator_type is float:
            return float(self.random_double_step(self.seed))
        else:
            raise StopIteration

    def __iter__(self) -> __name__:
        return self

    def __reversed__(self):
        return NotImplemented
